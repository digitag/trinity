<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <?php include( 'layout/head.php'); ?>
    <style>
        h3 {font-size: 21px}
        .grid figure {cursor: auto}
    </style>
    <title>UAE - Trinity Group</title>
</head>

<body>
    <div class="container-fluid">
        <?php include( 'layout/header.php'); ?>
    </div>

    <div class="container" style="margin-top:60px">
        <h1>UNITED ARAB EMIRATES</h1>
        <img style="margin-right:20px" class="img-responsive pull-left" src="HoverEffectIdeas/img/uae-flag.jpg" width="200" height="124" alt="UAE">
        <p>With business set up options including Limited Liability Companies, Branch & Representative Offices, Free Zone and Offshore registration Trinity assists clients and advisors in choosing the appropriate option for their business requirements. The business activity of the client will dictate which of the options listed here will be the most appropriate. We work with clients in developing business plans that best represent their needs.</p>
    </div>


    <div class="grid" id="about1" style="margin:50px 0 0 0;height:650px;padding-top:200px;" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="150">
        <div class="container">
            <div class="popup-container big">Set up your company in UAE from 18.500 USD all inclusive<span>UAE residency visa included</span></div>
            <div class="popup-container small"><a href="#" data-toggle="modal" data-target="#myModal">Request info</a></div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="company-box">
                        <h3>Limited Liability Company</h3>
                        <p>This type of company allows for a joint venture between a UAE national and foreign partners. Expatriate partners are permitted to hold shares not exceeding 49% of the capital, with the UAE national partner holding the remaining 51%.</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="company-box">
                        <h3>Branch and Representative office</h3>
                        <p>Foreign companies may establish a branch or representative office in the UAE that may carry out promotional activities and facilitate contracts within UAE, however they are not allowed to manufacture or sell goods locally.</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="company-box">
                        <h3>Free Zone Registration </h3>
                        <p>The UAE has over 30 Free Zones where 20 are located in Dubai. The following types of registration are available in the Free Zones: Free Zone Establishment, Free Zone Company and Branch of a Foreign / Local Company</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="company-box">
                        <h3>Offshore Company </h3>
                        <p>There are two different kind of Offshore Company: Jebel Ali Offshore Companies and Ras Al Khaimah- International Company (RAK-IC). </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <button style="margin-bottom:50px" type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
        REQUEST INFORMATION
    </button>

    <div class="row" id="cit" style="margin:0" data-stellar-background-ratio="0.5">
        <div class="container" style="text-align:center;background-color:rgba(0, 0, 0, 0.5);padding:50px">
            <p style="color:#fff"><em>“Professionalism, precision and reliability...these are the first things that come to mind when we think of Trinity. As one of our main independent counterparts, they have enabled our clients to rely on their know-how and expertise to achieve their wishes.”</em>
            </p>

            <p style="color:#fff"><strong>Riccardo Romani<br>
 Mind Advisors SA </strong> </p>
        </div>
    </div>
    <script>
        $(window).load(function(){
            $('.popup-container.big').animate({
                left : 0
            },1000)
            $('.popup-container').animate({
                left : 0
            },1500)
        })
    </script>

    <?php include( 'layout/footer.php'); ?>
    <?php include( 'layout/form-request.php'); ?>
</body>

</html>