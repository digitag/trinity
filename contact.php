<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include('layout/head.php'); ?>
<title>Corporate solution for trade and investment - Trinity Group</title>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script src="js/map-contact.js"></script>
</head>

<body>
<div class="container-fluid">

<?php include('layout/header.php'); ?>

</div>

<div class="container" style="margin-top:60px">
<h1>CONTACT</h1>

<div class="row">
    <div class="col-md-6">
    <h2>Dubai</h2>
    <p>Office 1008, 10th Floor,<br>
International Business Tower, Business Bay<br>
P.O. Box 58562, Dubai, <strong>UAE</strong></p>
<p>T : +971 4 447 8931<br>
E : <a href="mailto:info@trinitygroup.eu">info@trinitygroup.eu</a><br>
F : +971 4 447 8924</p>
    </div>
    <div class="col-md-6">
    <h2>Zurich</h2>
    <p>Bahnhofstrasse 52,<br />
CH-8001 Zurich,<br />
<strong>Switzerland</strong></p>
<p>T : + 41 44 214 67 09<br>
E : <a href="mailto:info@trinitygroup.eu">info@trinitygroup.eu</a><br>
F : + 41 44 214 67 10</p>
    </div>
  </div> 
  

     
</div>


   	<div id="container-map" style="margin-top:60px">
  		<div id="top-sfu"></div>
    	<div id="map-canvas"></div>
    	<div id="bottom-sfu"></div>
  	</div>


<script src="js/bootstrap.min.js"></script>
</body>

</html>