<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php"><img src="img/logo.jpg" alt=""/></a>
    </div>
    <div class="collapse navbar-collapse cl-effect-4" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      	<li><a href="index.php">Home</a></li>
        <li><a href="about.php">About</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Services <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="corporate-solutions.php">Corporate solutions</a></li>
            <li><a href="trust-and-generation-planning.php">Trust & generation planning</a></li>
            <li><a href="property-ownership.php">Property ownership</a></li>
            <li><a href="banking-services.php">Banking services</a></li>
            <li><a href="sports-and-entertainment.php">Sports & entertainment</a></li>
          </ul>
        </li>
        <li><a href="uae.php">UAE</a></li>
        <li><a href="jurisdictions.php">Jurisdictions</a></li>
        <li><a href="news.php">news</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="https://twitter.com/trinitycorpserv" target="_blank"><img src="img/twitter.png" width="30" height="30" alt="Twitter Trinity Group"/></a></li>
        <li><a href="https://www.linkedin.com/company/2653219?trk=tyah&trkInfo=idx%3A1-1-1%2CtarId%3A1426159567722%2Ctas%3Atrinity+group+parth&trk=tyah&trkInfo=idx%3A1-1-1%2CtarId%3A1426159567722%2Ctas%3Atrinity+group+part" target="_blank"><img src="img/linkedin.png" width="30" height="30" alt=""/></a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
    </div>
  </div>
</nav>