<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Request information</h4>
            </div>
            <div class="modal-body">
                <form onsubmit="sendMail();">
                    <div class="form-group">
                        <labe>Name*</label>
                            <input type="text" class="form-control" name="name" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label>Email address*</label>
                        <input type="email" class="form-control" name="email" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="Mobile">Mobile</label>
                        <input type="tel" class="form-control" id="Mobile" name="tel" placeholder="Enter mobile">
                    </div>
                    <div class="form-group">
                        <label>Your message</label>
                        <textarea class="form-control" rows="3" name="message"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default" onclick="sendMail();">Submit</button>
            </div>
        </div>
    </div>
</div>
<script>
    var base_url = "http://local.whitestar.digitag.me";

    function loading() {}

    function stopLoading() {}

    function sendMail() {
        loading();
        var send_button = $('#myModal button[type=submit]');
        send_button.html('SENDING').attr('disabled', 'disabled');
        if ($('#myModal  input[name=email]').val().length == 0 || $('#myModal textarea[name=message]').val().length == 0) {
            send_button.removeAttr('disabled').html('SUBMIT');
            alert('Please fill in your email and the message!');
            return false;
        }
        $.post(base_url + "/api/send-email", {
            name: $('#myModal input[name=name]').val(),
            email: $('#myModal input[name=email]').val(),
            tel: $('#myModal input[name=tel]').val(),
            message: $('#myModal textarea[name=message]').val(),
        }, function (data) {
            if (data.response !== 'success') {
                alert(data.message);
                send_button.removeAttr('disabled').html('SUBMIT');
            } else {
                send_button.html('SENT').attr('disabled', 'disabled');
            }
            stopLoading();
        });
    }
</script>