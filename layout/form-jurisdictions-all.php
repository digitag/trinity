<div class="modal fade" id="info-other-all" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Request information</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Name*</label>
                        <input type="password" class="form-control" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label>Email address*</label>
                        <input type="email" class="form-control" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label>Mobile</label>
                        <input type="tel" class="form-control" placeholder="Enter mobile">
                    </div>
                    <div class="form-group" style="margin-left:30px">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> Bahamas
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox2" value="option2"> Belize
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox3" value="option3"> British Virgin Islands
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox4" value="option4"> Cayman Islands
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox5" value="option5"> Cyprus
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox6" value="option6"> Hong Kong
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox7" value="option7"> Isle of Man
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox8" value="option8"> Jersey
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox9" value="option9"> Malta
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox10" value="option10"> Mauritius
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox11" value="option11"> New Zealand
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox12" value="option12"> Panama
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox13" value="option13"> Seychelles
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox14" value="option14"> Singapore
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="inlineCheckbox15" value="option15"> Switzerland
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </div>
</div>