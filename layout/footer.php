<div class="container-fluid" id="contact">
    <div class="row">
        <div class="col-md-3 col-md-offset-1 col-sx-offset-0">
            <h1>Contact</h1>
        </div>
        <div class="col-md-3 col-md-offset-1 col-sx-offset-0">
            <h2>Dubai</h2>
            <p>Office 1008, 10th Floor,
                <br> International Business Tower, Business Bay
                <br> P.O. Box 58562, Dubai, <strong>UAE</strong>
            </p>
            <p>T : +971 4 447 8931
                <br> E : <a href="mailto:info@trinitygroup.eu">info@trinitygroup.eu</a>
                <br> F : +971 4 447 8924</p>
        </div>
        <div class="col-md-3 col-sx-offset-0">
            <h2>Zurich</h2>
            <p>Bahnhofstrasse 52,
                <br /> CH-8001 Zurich,
                <br />
                <strong>Switzerland</strong>
            </p>
            <p>T : + 41 44 214 67 09
                <br> E : <a href="mailto:info@trinitygroup.eu">info@trinitygroup.eu</a>
                <br> F : + 41 44 214 67 10</p>
        </div>
    </div>
</div>
<div class="container-fluid text-left" id="terms">
    <a class="home-visible" href="javascript:void(0)" data-toggle="modal" data-target="#privacyPolicy">Privacy Policy</a>
    <a class="home-visible" href="javascript:void(0)" data-toggle="modal" data-target="#termsConditions">Terms and conditions</a>
    <a class="home-visible" href="javascript:void(0)" data-toggle="modal" data-target="#Refund">Refund/ Return Policy</a>
    <p id="managed">www.trinitygroup.eu is owned and managed by Trinity Group Partners</p>
</div>

<div class="modal fade" id="privacyPolicy" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Privacy Policy</h4>
            </div>
            <div class="modal-body">
                <p>- All credit/debit cards details and personally identifiable information will NOT be stored, sold, shared, rented or leased to any third parties</p>

                <p>- The Website Policies and Terms & Conditions may be changed or updated occasionally to meet the requirements and standards. Therefore the Customers’ are encouraged to frequently visit these sections in order to be updated about the changes on the website. Modifications will be effective on the day they are posted.</p>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>


<div class="modal fade" id="termsConditions" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Terms and conditions</h4>
            </div>
            <div class="modal-body">
                <p>- We accept payments online using Visa and MasterCard credit/debit card in AED (or any other currency).</p>
                <p>- Any dispute or claim arising out of or in connection with this website shall be governed and construed in accordance with the laws of UAE.</p>
                <p>- United Arab of Emirates is our country of domicile.</p>
                <p>- www.trinitygroup.eu will NOT deal or provide any services or products to any of OFAC sanctions countries in accordance with the law of UAE. </p>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="Refund" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" >Refund/ Return Policy</h4>

            </div>
            <div class="modal-body">
                <p>It is generally not our policy to return professional fees for the work we are asked to carry out. We will however return government fees and other specific disbursements, for example license and sponsor fees (where possible) in the event that we are unable to complete the job at hand due to circumstances beyond our control.</p>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<script src="js/bootstrap.min.js"></script>