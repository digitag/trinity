<div class="modal fade" id="info-other" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="form-jd-title">Request information <span></span></h4>
      </div>
      <div class="modal-body">
<form>
  <div class="form-group">
    <label for="exampleInputPassword1">Name*</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Name">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address*</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
  </div>
  <div class="form-group">
    <label for="Mobile">Mobile</label>
    <input type="tel" class="form-control" id="Mobile" placeholder="Enter mobile">
  </div>
   <input type="hidden" name="requested_city" value="" />
</form>
      </div>
      <div class="modal-footer">
     
      <button type="submit" class="btn btn-default">Submit</button>
</div>
    </div>
  </div>
</div>