<meta name="viewport" content="width=device-width, initial-scale=1">
<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<link href="favicon.ico" type="image/x-icon" rel="shortcut icon">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/component.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/stellar.js"></script>
	<script>
		$(function(){
			$.stellar({
				horizontalScrolling: false,
				verticalOffset: 0
			});
		});
		
	</script>
   
<link rel="stylesheet" type="text/css" href="HoverEffectIdeas/css/demo.css" />
<link rel="stylesheet" type="text/css" href="HoverEffectIdeas/css/set2.css" />
<script src="js/jquery.scrollTo.min.js"></script>