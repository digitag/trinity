<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include('layout/head.php'); ?>
    
<title>Services - Trinity Group</title>

</head>

<body>
<div class="container-fluid">

<?php include('layout/header.php'); ?>

</div>

<div class="container" style="margin-top:60px">
<h2 style="font-size:4em;padding:100px 0 30px 0">SERVICES</h2>
<div class="container"><p>Pellentesque efficitur elit et nisi semper pharetra. Etiam ac lectus eu sapien tristique tempor.</p>
<p style="margin-bottom:50px">Curabitur pellentesque tempor arcu. Nam nisl lacus, dapibus vitae justo sit amet, commodo pulvinar sem. Donec pharetra finibus lectus, id auctor ante fermentum eu. </p>

<div class="col-sm-3 text-center">
<img src="../trinity/2/ico/01.png" width="140" height="140">
<p>LOREM IPSUM</p>
</div>
<div class="col-sm-3 text-center">
<img src="../trinity/2/ico/02.png" width="140" height="140">
<p>DOLOR SIT</p>
</div>
<div class="col-sm-3 text-center">
<img src="../trinity/2/ico/03.png" width="140" height="140">
<p>CONSECTUR ADIPIS</p>
</div>
<div class="col-sm-3 text-center">
<img src="../trinity/2/ico/04.png" width="140" height="140">
<p>ELIT TITIRE</p>
</div>

</div>
</div>




<div class="container" style="margin-top:60px">

<h2>IMPERDIET LACINIA NISL</h2>
<p>Praesent neque erat, placerat vel suscipit at, bibendum non diam. Pellentesque ut tempus purus, eu vehicula tellus. Maecenas placerat sem eu leo euismod rhoncus. Sed imperdiet lacinia nisl, non elementum sapien hendrerit eu. Cras et ultrices urna.</p>
<h2>DONEC FRINGILLA</h2>
<p style="margin-bottom:40px">Etiam ullamcorper nulla sed arcu iaculis posuere. Praesent tincidunt mollis tellus, ut mollis erat commodo id. Duis sodales pharetra nibh, volutpat tincidunt ligula. Donec fringilla nibh sit amet urna ultricies consequat. Etiam lacinia, felis non vulputate feugiat, felis leo aliquet tellus. </p>
<ul class="list">
<li>Donec pharetra finibus</li>
<li>Nam nisl lacus dapibus vitae</li>
<li>Etiam ac lectus eu sapien</li>
</ul>

<h2>FUSCE QUIS PLACERAT LEO</h2>
<p style="margin-bottom:40px">Vestibulum in blandit augue. Proin imperdiet dui eu justo egestas, eget luctus ante vehicula. Etiam ullamcorper nulla sed arcu iaculis posuere.  Nulla porta, odio ac vestibulum semper, erat leo cursus magna, at facilisis libero dolor id diam. </p>
<p style="margin-bottom:50px"><strong>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce quis placerat leo, egestas eleifend lectus.</strong></p>

</div>

<div class="row" id="about1" style="margin:0" data-stellar-background-ratio="0.5">
</div>
    
<?php include('layout/footer.php'); ?>

</body>

</html>