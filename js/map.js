function addNation(lat,long,name,map){
		  var contentString = 
		      '<div class="popup">'+
		      '<h3> '+name+' </h3>'+
			  '<button type="button" class="btn btn-primary btn-lg map-dialog" data-toggle="modal" onclick="showDialog(\'' + name + '\')">Request information</button>'+
		      '</div>';
		      
		  var infowindow = new google.maps.InfoWindow({
		      content: contentString,
		      maxWidth: 230,
		      maxHeight: 300,
		
		  });
		
		  var myLatLng = new google.maps.LatLng(lat, long);
		  var marker = new google.maps.Marker({
		      position: myLatLng,
		      map: map,
		      icon: imagePointer
		  });
		  
		  google.maps.event.addListener(marker, 'click', function() {
		    infowindow.open(map,marker);
		  });
}
function initialize() {
		
		/* Style of the map */
		var styles = [
		{
		  stylers: [
			{ hue: "#5489A8" },
			{ saturation: -80 }
		  ]
		},{
		  featureType: "road",
		  elementType: "geometry",
		  stylers: [
			{ lightness: 100 },
			{ visibility: "simplified" }
		  ]
		},{
		  featureType: "road",
		  elementType: "labels",
		  stylers: [
			{ visibility: "off" }
		  ]
		},{
		  featureType: "administrative.country",
		  elementType: "all",
		  stylers: [
			{ visibility: "off" }
		  ]
		},{
			 featureType: "poi",
			 elementType: "labels",
			 stylers: [
			   { visibility: "off" }
			 ]
		   }
	
		 ];
		 
  // Create a new StyledMapType object, passing it the array of styles,
	  // as well as the name to be displayed on the map type control.
	  var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
	  
	  /* Lat. and Lon. of the center of the map */
	  var myCenter = new google.maps.LatLng(40.178152, 55.267345);
	  
	  // Create a map object, and include the MapTypeId to add
	  // to the map type control.
	  var mapOptions = {
		zoom: 3, 				//zoom level
		center: myCenter, 		//center position
		scrollwheel: false, 	//zoom when scroll disable
		zoomControl: true, 		//show control zoom
	   
		mapTypeControlOptions: {
		  mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style'] 
		}
	
	  };
	  
	  var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
	
	  //Associate the styled map with the MapTypeId and set it to display.
	  map.mapTypes.set('map_style', styledMap);
	  map.setMapTypeId('map_style');
	  
	  
	  /* Marker Dubai */
	  var contentString = 
		  '<div class="popup">'+
		  '<h2 id="dubai">Dubai</h2>'+
		  '<p>Office 1008,  10th Floor,<br/>'+
		  'International Business Tower<br/>'+
			'Business Bay</p>'+
		'<a target="_blank" href="https://www.google.it/maps?q=trinity+group+dubai&oe=utf-8&gws_rd=cr&um=1&ie=UTF-8&sa=X&ei=2wIMVYSMJ8nrUrW4hLgM&ved=0CAkQ_AUoAw">'+
		      'Watch the Full Screen Map &#187;</a> '+

		  '</div>';
	
	  var infowindow = new google.maps.InfoWindow({
		  content: contentString,
		  maxWidth: 230,
		  maxHeight: 300,
	
	  });
	  
	  var myLatlng = new google.maps.LatLng(25.178152, 55.267345);
	  var marker_dubai = new google.maps.Marker({
		  position: myLatlng,
		  map: map,
		  title: 'Uluru (Ayers Rock)'
	  });
	 
	  google.maps.event.addListener(marker_dubai, 'click', function() {
		infowindow.open(map,marker_dubai);
	  });
	  
	   /* open popup marker when map is load */
	  new google.maps.event.trigger( marker_dubai, 'click' );  		






	  /* Marker Zurich */
	  var contentString = 
		  '<div class="popup">'+
		  '<h2 id="zurich">Zurich</h2>'+
		  '<p>Bahnhofstrasse 52,<br/>'+
		  'CH-8001 Zurich,<br/>'+
			'Switzerland</p>'+
		'<a target="_blank" href="https://www.google.it/maps/place/Bahnhofstrasse+52,+8001+Z%C3%BCrich,+Svizzera/@47.3728331,8.5384716,17z/data=!3m1!4b1!4m2!3m1!1s0x47900a06c32cf343:0xced839330be2625c">'+
		      'Watch the Full Screen Map &#187;</a> '+
		  '</div>';
	
	  var infowindow = new google.maps.InfoWindow({
		  content: contentString,
		  maxWidth: 230,
		  maxHeight: 300,
	
	  });
	  
	  var myLatlng = new google.maps.LatLng(47.368504, 8.537402);
	  var marker_zurich = new google.maps.Marker({
		  position: myLatlng,
		  map: map,
		  title: 'Uluru (Ayers Rock)'
	  });
	 
	  google.maps.event.addListener(marker_zurich, 'click', function() {
		infowindow.open(map,marker_zurich);
	  });
	  
	   /* open popup marker when map is load */
	  new google.maps.event.trigger( marker_zurich, 'click' );  		


	
	addNation(24.583248, -77.878908, 'Bahamas',map);
	addNation(17.429399, -88.524452, 'Belize',map);
	addNation(18.427535, -64.622510, 'British Virgin Islands',map);
	addNation(19.311230, -81.236588, 'Cayman Islands',map);
	addNation(35.024809, 33.170183, 'Cyprus',map);
	addNation(22.271918, 114.183490, 'Hong Kong',map);
	addNation(54.222374, -4.549893, 'Isle of Man',map);
	addNation(49.216621, -2.137783, 'Jersey',map);
	addNation(35.951522, 14.389824, 'Malta',map);
	addNation(-20.235000, 57.565637, 'Mauritius',map);
	addNation(-42.586838, 172.554161, 'New Zealand',map);
	addNation(9.199049, -79.572528, 'Panama',map);
	addNation(-4.678754, 55.468704, 'Seychelles',map);
	addNation(1.277937, 103.838543, 'Singapore',map);
	addNation(46.931488, 8.130826, 'Switzerland',map);
	
}
var imagePointer = 'map/img/pointer.png';
google.maps.event.addDomListener(window, 'load', initialize);
