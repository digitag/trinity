<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include('layout/head.php'); ?>
<style>h3{font-size:21px}
.grid figure{cursor:auto}
.btn-primary{background-color:transparent;border:none}
.btn-primary:hover,.btn-primary:focus{background-color:rgba(255, 255, 255, 0.2)}
    </style>
<title>Jurisdictions - Trinity Group</title>
</head>

<body>
<div class="container-fluid">

<?php include('layout/header.php'); ?>

</div>

<div class="container" style="margin-top:60px">
<h1>OTHER JURISDICTIONS</h1>
<p style="margin-bottom:40px">Faced with numerous and sometimes confusing incorporation options our aim is to assist clients in selecting an appropriate jurisdiction by taking a collaborative approach to their business requirements. The jurisdictions highlighted below are by no means exhaustive but in each case we have set out the characteristics of company incorporation in some of the world's more well known offshore and onshore financial centres.</p>
</div>



<div class="container-fluid">
<div class="grid">
  
  					<figure class="effect-selena">
                    	<img src="HoverEffectIdeas/img/bahamas.jpg" alt="img10"/>
						<figcaption>
						  <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>BAHAMAS</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/belize.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>BELIZE</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/virgin-island.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>BRITISH VIRGIN ISLANDS</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/cayman.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>CAYMAN ISLANDS</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/cyprus.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>CYPRUS</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/hong-kong.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>HONG KONG</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/man.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>ISLE OF MAN</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/jersey.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>JERSEY</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/malta.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>MALTA</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/mauritius.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>MAURITIUS</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/new-zealand.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>NEW ZEALAND</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/panama.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>PANAMA</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/seychelles.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>SEYCHELLES</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/singapore.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>SINGAPORE</h2></button>
						</figcaption>			
					</figure>
                    <figure class="effect-selena">
						<img src="HoverEffectIdeas/img/switzerland.jpg" alt="img10"/>
						<figcaption>
							<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#info-other"><h2>SWITZERLAND</h2></button>
						</figcaption>			
					</figure>
					
<div class="clearfix"></div>    
                
	</div></div>

<button type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#info-other-all">
 REQUEST INFORMATION
</button>


  <?php include('layout/footer.php'); ?>
  <?php include('layout/form-jurisdictions.php'); ?>
  <?php include('layout/form-jurisdictions-all.php'); ?>
  <script src="js/main.js"></script>

</body>

</html>