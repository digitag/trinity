
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include('layout/head.php'); ?>
<title>Sports and entertainment - Trinity Group</title>

</head>

<body>
<div class="container-fluid">

<?php include('layout/header.php'); ?>

</div>

<div class="container" style="margin-top:60px">
<h1>Sports and<br>
ENTERTAINMENT</h1>
<div class="container">
<p>“Form is temporary, class is permanent” so the saying goes.</p>
<p style="margin-bottom:50px">Whilst few would argue with that statement it is also understood that the careers of most international sports men and women are relatively short. Maximising earning potential is therefore of paramount importance to international athletes and entertainers.</p>
</div>
</div>

<div class="row" id="about1" style="margin:0 0 50px 0" data-stellar-background-ratio="0.5"></div>
<div class="container">

<button style="margin-bottom:50px" type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
 REQUEST INFORMATION
</button>

    <p>For information and advice on salary enhancement and protection please <strong>contact us</strong>.</p>
</div>


  <?php include('layout/footer.php'); ?>
  <?php include('layout/form-request.php'); ?>
</body>

</html>