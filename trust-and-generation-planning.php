<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include('layout/head.php'); ?>
<title>Trust and generation planning - Trinity Group</title>

</head>

<body>
<div class="container-fluid">

<?php include('layout/header.php'); ?>

</div>

<div class="container" style="margin-top:60px">
<h1>Trust & generation planning</h1>
<div class="container">
<p>All too often insufficient consideration is given to how assets, particularly those held overseas, can be passed from one generation to the next.</p>
<p style="margin-bottom:50px">Trinity can advise on how a correctly drafted Trust arrangement can provide continuity of ownership and a seamless transfer of assets to chosen beneficiaries without the inconvenience of probate, forced heirship and / or inheritance taxes.</p>
</div>
</div>

<div class="row" id="about1" style="margin:0 0 50px 0" data-stellar-background-ratio="0.5"></div>
<div class="container">

<button style="margin-bottom:50px" type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
 REQUEST INFORMATION
</button>

    <p>For specific advice on how we can assist please <strong>contact us</strong>.</p>
</div>


  <?php include('layout/footer.php'); ?>
  <?php include('layout/form-request.php'); ?>
</body>

</html>