<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include('layout/head.php'); ?>
<title>Press - Trinity Group</title>
<style>.btn-lg, .btn-group-lg > .btn{margin-top:10px}</style>
</head>

<body>
<div class="container-fluid">

<?php include('layout/header.php'); ?>

</div>

<div class="container" style="margin-top:60px">
<h1>News</h1>

<div class="row">
<div class="col-md-4 news">
<h3>Lorem ipsum dolor</h3>
<p>Curabitur lacinia consectetur felis, vel rutrum lorem viverra a. Duis placerat pulvinar risus, at mattis elit vulputate non...</p>
<button type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
 VIEW MORE</button>
</div>
<div class="col-md-4 news">
<h3>Cras mattis, nunc gravida lobortis</h3>
<p> Vivamus id lacinia ligula, et tincidunt purus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae rhoncus urna...</p>
<button type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
 VIEW MORE</button>
</div>
<div class="col-md-4 news">
<h3>Fusce felis nulla, euismod eu lobortis</h3>
<p>Curabitur lacinia consectetur felis, vel rutrum lorem viverra a. Duis placerat pulvinar risus, at mattis elit vulputate non...</p>
<button type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
 VIEW MORE</button>
</div>

<div class="col-md-4 news">
<h3>Vestibulum quis sodales ligula.</h3>
<p>Aliquam nec arcu eu lorem finibus tempor non id risus. Suspendisse ligula diam, convallis eu tortor vel...</p>
<button type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
 VIEW MORE</button>
</div>
<div class="col-md-4 news">
<h3>Lorem ipsum dolor</h3>
<p>Vestibulum quis elit ante. Fusce felis nulla, euismod eu lobortis ac, porta ac ante...</p>
<button type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
 VIEW MORE</button>
</div>
<div class="col-md-4 news">
<h3>Fusce felis nulla, euismod eu lobortis</h3>
<p>Mauris vitae sodales mi, varius convallis est. Donec quis est turpis. Donec justo eros, gravida a dui in, dignissim hendrerit libero...</p>
<button type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
 VIEW MORE</button>
</div>

</div>
</div>

<div class="row hidden-xs" id="about1" style="margin:0" data-stellar-background-ratio="0.5"></div>




  <?php include('layout/footer.php'); ?>

  



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Lorem ipsum dolor</h4>
      </div>
      <div class="modal-body">
<p>Fusce eget nulla ac urna <strong>mattis semper nec vitae</strong> eros. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam a sem non elit egestas facilisis. Nam tristique arcu sed purus feugiat egestas. Proin dictum <strong>pellentesque eros nec fringilla</strong>. Cras eu gravida nunc, quis accumsan libero. Nullam aliquam augue at mollis vestibulum.</p>
<p>Donec ligula lorem, malesuada eget mattis non, condimentum sed magna. Nunc bibendum dictum aliquam. Nam porttitor leo eu nunc mollis ultrices. Nullam mattis malesuada viverra. Duis eget dolor vitae velit condimentum fringilla. Morbi eget lectus odio. </p>
      </div>
      <div class="modal-footer">  </div>
    </div>
  </div>
</div>
</body>

</html>