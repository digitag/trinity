<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <?php include( 'layout/head.php'); ?>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script src="js/map.js"></script>
    <script src="js/modernizr.custom.js"></script>

    <title>Trinity Group</title>
    <style>.home-visible {display:inline}</style>
</head>

<body>
    <div id="carousel-example-generic" class="carousel slide fool_screen" data-ride="carousel">
        <?php include( 'layout/header.php'); ?>
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner" role="listbox" id="home">
            <div class="item active fool_screen" id="bgh1">
                <div class="carousel-caption" onclick="$.scrollTo('#services',800, {offset: {top:-130} })">
                    <h1>Specialists in Trust &amp; Corporate Services</h1>
                </div>
            </div>
            <div class="item fool_screen" id="bgh2">
                <div class="carousel-caption" onclick="window.location='uae.php'">
                    <h1>Local, Free Zone, Offshore company formation</h1>
                </div>
            </div>
            <div class="item fool_screen" id="bgh3">
                <div class="carousel-caption" onclick="window.location='jurisdictions.php'">
                    <h1>Business set up solutions</h1>
                </div>
            </div>
        </div>

    </div>

    <div class="container-fluid" style="text-align:center">
        <h2 class="h2-home">ABOUT US</h2>
        <p style="margin-bottom:30px;font-size:21px">Trinity Group Partners specialise in company formation worldwide but with a specific focus on the United Arab Emirates.</p>
        <p style="margin-bottom:50px"><a class="link-more" href="about.php">VIEW MORE</a>
        </p>
    </div>

    <div class="row grid hidden-xs" id="about" style="margin:0" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="-800"></div>

    </div>
    <div class="container-fluid" style="text-align:center" id="services">
        <h2 class="h2-home">OUR SERVICES</h2>
    </div>
    <div class="grid">
        <a href="corporate-solutions.php">
            <figure class="effect-selena"> <img src="HoverEffectIdeas/img/1h2.jpg" alt="img10" />
                <figcaption>
                    <h2>Corporate solutions</h2>
                </figcaption>
            </figure>
        </a>
        <a href="trust-and-generation-planning.php">
            <figure class="effect-selena">
                <img src="HoverEffectIdeas/img/2h2.jpg" alt="img10" />
                <figcaption>
                    <h2>Trust &amp; generation planning</h2>
                </figcaption>
            </figure>
        </a>
        <a href="property-ownership.php">
            <figure class="effect-selena">
                <img src="HoverEffectIdeas/img/3h2.jpg" alt="img10" />
                <figcaption>
                    <h2>Property ownership</h2>
                </figcaption>
            </figure>
        </a>
        <a href="banking-services.php">
            <figure class="effect-selena">
                <img src="HoverEffectIdeas/img/4h2.jpg" alt="img10" />
                <figcaption>
                    <h2>Banking Services</h2>
                </figcaption>
            </figure>
        </a>
        <a href="sports-and-entertainment.php">
            <figure class="effect-selena">
                <img src="HoverEffectIdeas/img/5h2.jpg" alt="img10" />
                <figcaption>
                    <h2>Sports &amp; Entertainment</h2>
                </figcaption>
            </figure>
        </a>

        <div class="clearfix"></div>

    </div>
    <button type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
        REQUEST INFORMATION
    </button>




    <div class="container-fluid" style="text-align:center">
        <h2 class="h2-home">WHY UAE?</h2>
    </div>

    <div class="row grid" id="investor" style="margin:0" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="-1000">
        <div class="container">
            <div class="col-sm-3">
                <h2>Limited Liability<br>
Company</h2>
            </div>
            <div class="col-sm-3">
                <h2>Branch and <br> Representative office</h2>
            </div>
            <div class="col-sm-3">
                <h2>Free zone<br>
Registration</h2>
            </div>
            <div class="col-sm-3">
                <h2>Offshore<br>
Company</h2>
            </div>
        </div>
        <p style="margin-top:100px"><a class="link-more" href="uae.php">VIEW MORE</a>
        </p>
    </div>


    <div class="container-fluid text-center">
        <h2 class="h2-home">JURISDICTIONS</h2>
        <p style="margin-bottom:50px"><a class="link-more" href="jurisdictions.php">VIEW ALL</a>
        </p>
    </div>

    <div id="container-map">
        <div id="top-sfu"></div>
        <div id="map-canvas"></div>
        <div id="bottom-sfu"></div>
    </div>

    <?php include( 'layout/footer.php'); ?>
    <?php include( 'layout/form-request.php'); ?>
    <?php include( 'layout/form-jurisdictions.php'); ?>

    <script src="js/main.js"></script>

</body>

</html>