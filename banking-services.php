<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include('layout/head.php'); ?>
<title>Banking and investment services - Trinity Group</title>

</head>

<body>
<div class="container-fluid">

<?php include('layout/header.php'); ?>

</div>

<div class="container" style="margin-top:60px">
<h1>Banking services</h1>
<div class="container">
<p>Doing business overseas requires a quality working relationship with a bank that understands your needs.</p>
<p>To assist in that process, we advise clients in developing comprehensive business plans clearly setting out geographical areas of business activity, trading experience and revenue projections.</p>
<p style="margin-bottom:50px">For our investment clients we work closely with Swiss based Fund and Asset Managers in developing investment strategies tailored to specific client requirements.</p>
</div>
</div>

<div class="row" id="about1" style="margin:0 0 50px 0" data-stellar-background-ratio="0.5"></div>
<div class="container">

<button style="margin-bottom:50px" type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
 REQUEST INFORMATION
</button>

    <p>For further information please <strong>contact us</strong>.</p>
</div>


  <?php include('layout/footer.php'); ?>
  <?php include('layout/form-request.php'); ?>
</body>

</html>