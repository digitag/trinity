<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include('layout/head.php'); ?>
<title>Property ownership - Trinity Group</title>

</head>

<body>
<div class="container-fluid">

<?php include('layout/header.php'); ?>

</div>

<div class="container" style="margin-top:60px">
<h1>Property ownership</h1>
<div class="container">
<p>When investing overseas, precious little thought is given to the tax implications of buying, owning and selling property.</p>
<p style="margin-bottom:50px">Whether it is inheritance tax, capital gains tax or the vagaries of forced heirship, we strongly advise serious consideration be given to purchasing property via a corporate structure.</p>
</div>
</div>

<div class="row" id="about1" style="margin:0 0 50px 0" data-stellar-background-ratio="0.5"></div>
<div class="container">

<button style="margin-bottom:50px" type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
 REQUEST INFORMATION
</button>

    <p>For specific country focused advice, please <strong>contact us</strong>.</p>
</div>


  <?php include('layout/footer.php'); ?>
  <?php include('layout/form-request.php'); ?>


</body>

</html>