<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <?php include( 'layout/head.php'); ?>
    <title>About - Trinity Group</title>
    <style>
        #managed {
            display: block;
        }
    </style>
</head>

<body>
    <div class="container-fluid">

        <?php include( 'layout/header.php'); ?>

    </div>

    <div class="container" style="margin-top:60px">
        <h1>ABOUT US</h1>
        <p>Trinity Group Partners specialise in company formation worldwide but with a specific focus on the United Arab Emirates where company registration is at an all time high. With collective industry and regional experience spanning 35 years we are one of the leading independent consultancies assisting clients from all corners of the globe with business set up solutions in one of the world’s fastest growing economies.</p>
    </div>

    <div class="row hidden-xs" id="about1" style="margin:0" data-stellar-background-ratio="0.5"></div>

    <div class="container" style="margin-top:40px">
        <button type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#requestInfo">
            REQUEST INFORMATION
        </button>


    </div>

    <div class="row" id="cit" style="margin:0" data-stellar-background-ratio="0.5">
        <div class="container" style="text-align:center;background-color:rgba(0, 0, 0, 0.5);padding:50px">
            <p style="color:#fff"><em>“I have been working with Trinity since 2005 as a dedicated service provider of corporate and administrative services.<br>
Their knowledge and expertise have been invaluable and client care is first class.”</em>
            </p>

            <p style="color:#fff"><strong>Werner Berger<br>
Solidus Asset Management</strong> </p>
        </div>
    </div>

    <?php include( 'layout/footer.php'); ?>
    <div class="modal fade" id="requestInfo" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Request information</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Name and Surname</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Name and Surname">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Your message</label>
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>