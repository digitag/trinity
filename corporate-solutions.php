<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include('layout/head.php'); ?>
<title>Corporate solution for trade and investment - Trinity Group</title>

</head>

<body>
<div class="container-fluid">

<?php include('layout/header.php'); ?>

</div>

<div class="container" style="margin-top:60px">
<h1>Corporate solutions</h1>
<div class="container">
<p>New ventures in overseas markets require research and investment to ensure valuable time and expense is not wasted through poor strategy and planning.</p>
<p>Trinity advises clients and advisors alike on appropriate corporate structures tailored to specific business needs.</p>
<p style="margin-bottom:50px">Focusing on tax mitigation and operating costs, we assist clients in forming companies that best suit their requirements. </p>
</div>
</div>

<div class="row" id="about1" style="margin:0 0 50px 0" data-stellar-background-ratio="0.5"></div>
<div class="container">

<button style="margin-bottom:50px" type="button" class="btn btn-primary btn-lg link-more" data-toggle="modal" data-target="#myModal">
 REQUEST INFORMATION
</button>

    <p>For advice on Investment Holding Companies, Trading Companies, International Consultancy Vehicles and Intellectual Property Holding Companies, please <strong>contact us</strong>.</p>
</div>


  <?php include('layout/footer.php'); ?>
  <?php include('layout/form-request.php'); ?>

</body>

</html>